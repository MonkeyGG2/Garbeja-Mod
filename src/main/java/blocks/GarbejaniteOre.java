package blocks;

import java.util.Random;

import init.MyItems;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;

public class GarbejaniteOre extends Block {
	public GarbejaniteOre(String unlocalizedName) {
		super(Material.rock);
		this.setUnlocalizedName(unlocalizedName);
		this.setHardness(5.0F);
		this.setResistance(9999);
        this.setCreativeTab(CreativeTabs.tabBlock);
	}
	
	@Override
    public int getExpDrop(IBlockAccess world, BlockPos pos, int fortune)
    {
        if (this.getItemDropped(world.getBlockState(pos), RANDOM, fortune) != Item.getItemFromBlock(this))
        {
            return 10 + RANDOM.nextInt(15);
        }
        return 0;
    }

	
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
    	return MyItems.garbejanite;
    }
    
    public int quantityDropped(Random random)
    {
        return 2 + random.nextInt(5);
    }
    
    public int quantityDroppedWithBonus(int fortune, Random random)
    {
        return MathHelper.clamp_int(this.quantityDropped(random) + random.nextInt(fortune + 2), 1, 6);
    }
    
    public MapColor getMapColor(IBlockState state)
    {
        return MapColor.stoneColor;
    }
}

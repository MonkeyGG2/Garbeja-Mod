package blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;

public class GarbejaniteBlock extends Block {
	public GarbejaniteBlock(String unlocalizedName) {
		super(Material.rock);
		this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(CreativeTabs.tabBlock);
        this.setHardness(20.0F);
        this.setHarvestLevel("pickaxe", 3);
	}
	
	public MapColor getMapColor(IBlockState state)
    {
        return MapColor.grayColor;
    }

}

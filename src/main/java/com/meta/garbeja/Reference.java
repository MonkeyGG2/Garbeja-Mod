package com.meta.garbeja;

public class Reference {
	public static final String MOD_ID = "garbeja";
	public static final String MOD_NAME = "Garbeja Mod";
	public static final String VERISON = "1.1";
	public static final String CLIENT_PROXY_CLASS = "proxy.ClientProxy";
	public static final String SERVER_PROXY_CLASS = "proxy.CommonProxy";

}


package com.meta.garbeja;

import handlers.RecipeHandler;
import init.MyBlocks;
import init.MyItems;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import proxy.CommonProxy;
import worldgen.OreGen;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERISON)
public class Garbeja {
	@SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		MyItems.init();
		MyItems.register();
		MyBlocks.init();
		MyBlocks.register();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {	
		proxy.registerRenders();
		GameRegistry.registerWorldGenerator(new OreGen(), 0);
		RecipeHandler.registerCraftingRecipes();
		RecipeHandler.registerFurnaceRecipes();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		
	}
}

package handlers;

import java.util.Random;

import init.MyBlocks;
import init.MyItems;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class RecipeHandler {
	public static void registerCraftingRecipes() {
		// Items -> Block
		
		GameRegistry.addRecipe(new ItemStack(MyBlocks.garbejanite_block), new Object[] {"III", "III", "III", 'I', MyItems.garbejanite});
		GameRegistry.addRecipe(new ItemStack(MyBlocks.crayzeja_block), new Object[] {"III", "III", "III", 'I', MyItems.crayzeja_ingot});
		
		// Block -> Items
		
		GameRegistry.addShapelessRecipe(new ItemStack(MyItems.garbejanite, 9), new Object[] {Item.getItemFromBlock(MyBlocks.garbejanite_block)});
		GameRegistry.addShapelessRecipe(new ItemStack(MyItems.crayzeja_ingot, 9), new Object[] {Item.getItemFromBlock(MyBlocks.crayzeja_block)});
		
		// Tools
		registerToolRecipes(MyItems.crayzeja_sword, MyItems.crayzeja_pickaxe, MyItems.crayzeja_axe, MyItems.crayzeja_shovel,  MyItems.crayzeja_hoe);
	}
	
	public static void registerFurnaceRecipes() {
		GameRegistry.addSmelting(MyItems.garbejanite, new ItemStack(MyItems.crayzeja_ingot), 10F);
		GameRegistry.addSmelting(MyBlocks.garbejanite_block, new ItemStack(MyBlocks.crayzeja_block), 100F);
	}
	
	public static void registerArmorRecipes() {
		
	}
	
	public static void registerToolRecipes(Item sword, Item pickaxe, Item axe, Item shovel, Item hoe) {
		// Sword
		ItemStack givenSword = new ItemStack(sword);
		
		GameRegistry.addRecipe(givenSword, new Object[] {" I ", " I ", " S ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		GameRegistry.addRecipe(givenSword, new Object[] {"I  ", "I  ", "S  ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		GameRegistry.addRecipe(givenSword, new Object[] {" I", " I", " S", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		
		// Pickaxe
		ItemStack givenPickaxe = new ItemStack(pickaxe);

		GameRegistry.addRecipe(givenPickaxe, new Object[] {"III", " S ", " S ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		
		// Axe
		ItemStack givenAxe = new ItemStack(axe);
		
		GameRegistry.addRecipe(givenPickaxe, new Object[] {" II", " SI", " S ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		GameRegistry.addRecipe(givenPickaxe, new Object[] {"II ", "IS ", " S ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		
		// Shovel
		ItemStack givenShovel = new ItemStack(shovel);
		
		GameRegistry.addRecipe(givenShovel, new Object[] {" I ", " S ", " S ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		GameRegistry.addRecipe(givenShovel, new Object[] {"I  ", "S  ", "S  ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		GameRegistry.addRecipe(givenShovel, new Object[] {"  I", "  S", "  S", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		
		// Hoe
		ItemStack givenHoe = new ItemStack(hoe);
		
		GameRegistry.addRecipe(givenHoe, new Object[] {" II", " S ", " S ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
		GameRegistry.addRecipe(givenHoe, new Object[] {"II ", " S ", " S ", 'I', MyItems.crayzeja_ingot, 'S', Items.stick});
	}
}

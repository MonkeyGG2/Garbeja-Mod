package tools;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.world.World;

public class CrayzejaSword extends ItemSword {
	
	public CrayzejaSword(ToolMaterial material, String unlocalizedName) {
		super(material);
		this.setUnlocalizedName(unlocalizedName);
	}
	
	@Override
    public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
    	if (isSelected && stack.getEnchantmentTagList().tagCount() == 0) {
    		stack.addEnchantment(Enchantment.sharpness, 10);
    		stack.addEnchantment(Enchantment.smite, 10);
    		stack.addEnchantment(Enchantment.baneOfArthropods, 10);
    		stack.addEnchantment(Enchantment.fireAspect, 10);
    		stack.addEnchantment(Enchantment.looting, 15);
    		stack.addEnchantment(Enchantment.unbreaking, 50);

    	}
    }
}

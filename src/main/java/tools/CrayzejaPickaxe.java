package tools;

import java.util.Random;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class CrayzejaPickaxe extends ItemPickaxe {
	
	public CrayzejaPickaxe(ToolMaterial material, String unlocalizedName) {
		super(material);
		this.setUnlocalizedName(unlocalizedName);
		this.setHarvestLevel("pickaxe", 4);
	}
	
	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		if (isSelected && stack.getEnchantmentTagList().tagCount() == 0) {
			stack.addEnchantment(Enchantment.efficiency, 15);
			stack.addEnchantment(Enchantment.unbreaking, 50);
			
			Random random = new Random();
			if (random.nextInt(2) == 1) {
				stack.addEnchantment(Enchantment.fortune, 15);
			}
			else {
				stack.addEnchantment(Enchantment.silkTouch, 69);
			}

		}
	}
}

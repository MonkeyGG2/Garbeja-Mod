package tools;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class CrayzejaAxe extends ItemAxe {
	
	public CrayzejaAxe(ToolMaterial material, String unlocalizedName) {
		super(material);
		this.setUnlocalizedName(unlocalizedName);
		this.setHarvestLevel("axe", 4);
	}
	
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		if (isSelected && stack.getEnchantmentTagList().tagCount() == 0) {
			stack.addEnchantment(Enchantment.efficiency, 15);
			stack.addEnchantment(Enchantment.silkTouch, 69);
			stack.addEnchantment(Enchantment.unbreaking, 50);
		}
	}

}
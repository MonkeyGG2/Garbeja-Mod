package init;

import com.meta.garbeja.Reference;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemSword;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import tools.CrayzejaAxe;
import tools.CrayzejaHoe;
import tools.CrayzejaPickaxe;
import tools.CrayzejaShovel;
import tools.CrayzejaSword;

public class MyItems {	
	// Tools
	
	public static ToolMaterial crayzeja_material = EnumHelper.addToolMaterial("crayzeja", 5, 5621, 16.0F, 5.0F, 30);
	public static ItemSword crayzeja_sword;
	public static ItemPickaxe crayzeja_pickaxe;
	public static ItemAxe crayzeja_axe;
	public static ItemSpade crayzeja_shovel;
	public static ItemHoe crayzeja_hoe;
	
	// Items
	
	public static Item garbejanite;
	public static Item crayzeja_ingot;
		
	public static void init() {
		// Tools
		
		crayzeja_sword = new CrayzejaSword(crayzeja_material, "crayzeja_sword");
		crayzeja_pickaxe = new CrayzejaPickaxe(crayzeja_material, "crayzeja_pickaxe");
		crayzeja_axe = new CrayzejaAxe(crayzeja_material, "crayzeja_axe");
		crayzeja_shovel = new  CrayzejaShovel(crayzeja_material, "crayzeja_shovel");
		crayzeja_hoe = new CrayzejaHoe(crayzeja_material, "crayzeja_hoe");
		
		// Items
		
		garbejanite = new Item().setUnlocalizedName("garbejanite");
		crayzeja_ingot = new Item().setUnlocalizedName("crayzeja_ingot");
	}
	
	public static void register() {
		// Tools
		
		GameRegistry.registerItem(crayzeja_sword, crayzeja_sword.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(crayzeja_pickaxe, crayzeja_pickaxe.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(crayzeja_axe, crayzeja_axe.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(crayzeja_shovel, crayzeja_shovel.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(crayzeja_hoe, crayzeja_hoe.getUnlocalizedName().substring(5));

		// Items
		
		GameRegistry.registerItem(garbejanite, garbejanite.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(crayzeja_ingot, crayzeja_ingot.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders() {
		registerRender(crayzeja_sword);
		registerRender(garbejanite);
		registerRender(crayzeja_ingot);
		registerRender(crayzeja_pickaxe);
		registerRender(crayzeja_axe);
		registerRender(crayzeja_shovel);
		registerRender(crayzeja_hoe);
	}
	
	public static void registerRender(Item item) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}

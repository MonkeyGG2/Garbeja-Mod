package init;

import com.meta.garbeja.Reference;

import blocks.CrayzejaBlock;
import blocks.GarbejaniteBlock;
import blocks.GarbejaniteOre;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class MyBlocks {
	public static Block garbejanite_ore;
	public static Block garbejanite_block;
	public static Block crayzeja_block;
	
	public static void init() {
		garbejanite_ore = new GarbejaniteOre("garbejanite_ore");
		garbejanite_block = new GarbejaniteBlock("garbejanite_block");
		crayzeja_block = new CrayzejaBlock("crayzeja_block");
	}
	
	public static void register() {
		registerBlock(garbejanite_ore);
		registerBlock(garbejanite_block);
		registerBlock(crayzeja_block);
	}
	
	public static void registerRenders() {
		registerRender(garbejanite_ore);
		registerRender(garbejanite_block);
		registerRender(crayzeja_block);
	}
	
	public static void registerBlock(Block block) {
		GameRegistry.registerBlock(block, block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRender(Block block) {
		Item item = Item.getItemFromBlock(block);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}

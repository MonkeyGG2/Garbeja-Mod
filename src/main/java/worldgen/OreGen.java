package worldgen;

import java.util.Random;

import com.google.common.base.Predicate;

import init.MyBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

public class OreGen implements IWorldGenerator {
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch (world.provider.getDimensionId()) {
		case 0: // Overworld
			generateOverworld(world, random, chunkX, chunkZ);
			break;
		case 1: // The End
			generateEnd(world, random, chunkX, chunkZ);
			break;
		case -1: // The Nether
			generateNether(world, random, chunkX, chunkZ);
			break;
		}
	}

	public void generateOverworld(World world, Random random, int chunkX, int chunkZ) {
		generateOre(MyBlocks.garbejanite_ore, world, random, chunkX, chunkZ, 1, 3, 5, 0, 12, Blocks.stone);
	}

	public void generateEnd(World world, Random random, int chunkX, int chunkZ) {

	}

	public void generateNether(World world, Random random, int chunkX, int chunkZ) {

	}

	public void generateOre(Block block, World world, Random random, int chunkX, int chunkZ, int minVeinSize, int maxVeinSize, int chance, int minY, int maxY, Block generateIn) {
		int veinSize = minVeinSize + random.nextInt(maxVeinSize);
		int heightRange = maxY - minY;
		WorldGenMinable gen = new WorldGenMinable(block.getDefaultState(), veinSize);
		for (int i = 0; i < chance; i++) {
			int xRand = chunkX * 16 + random.nextInt(16);
			int yRand = random.nextInt(heightRange) + minY;
			int zRand = chunkZ * 16 + random.nextInt(16);
			gen.generate(world, random, new BlockPos(xRand, yRand, zRand));
		}
	}
}

package proxy;

import init.MyBlocks;
import init.MyItems;

public class ClientProxy extends CommonProxy {
	@Override
	public void registerRenders() {
		MyItems.registerRenders();
		MyBlocks.registerRenders();
	}
}

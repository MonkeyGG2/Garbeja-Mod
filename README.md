# Garbeja-Mod

# Instructions
For installation, download the [Forge 1.8.9-11.15.1.2318 installer](https://files.minecraftforge.net/net/minecraftforge/forge/index_1.8.9.html), and install Forge for Client. Download _garbeja-[version].jar_ from the Releases, and move that to your mods folder. 

On Windows, your mod folder is located at:

    C:\Users\[user]\AppData\Roaming\.minecraft\mods
  
On OS X, your mod folder is located at:

    ~/Library/Application Support/minecraft/mods
  
On Linux, your mod folder is located at:

    ~/.minecraft/mods

Open Minecraft Launcher, create a new Installation, and set the Version to `release 1.8.9-forge1.8.9-11.15.1.2318-1.8.9`. Save the installation, and launch the game. Create a new world for Garbejanite Ore to generate, or load new chunks in an existing world.
